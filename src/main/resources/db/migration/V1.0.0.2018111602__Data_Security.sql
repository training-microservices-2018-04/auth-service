insert into s_permission (id, permission_label, permission_value) values
('p001', 'Edit Subjek', 'EDIT_SUBJEK');

insert into s_permission (id, permission_label, permission_value) values
('p002', 'Lihat Subjek', 'VIEW_SUBJEK');

insert into s_permission (id, permission_label, permission_value) values
('p003', 'Edit Peserta', 'EDIT_PESERTA');

insert into s_permission (id, permission_label, permission_value) values
('p004', 'Lihat Peserta', 'VIEW_PESERTA');

insert into s_role (id, name, description) values
('r001', 'Manager', 'Manager Level');

insert into s_role (id, name, description) values
('r002', 'Staff', 'Staff Level');

insert into s_role_permission (id_role, id_permission) values
('r001', 'p001');

insert into s_role_permission (id_role, id_permission) values
('r001', 'p002');

insert into s_role_permission (id_role, id_permission) values
('r001', 'p003');

insert into s_role_permission (id_role, id_permission) values
('r001', 'p004');

insert into s_role_permission (id_role, id_permission) values
('r002', 'p002');

insert into s_role_permission (id_role, id_permission) values
('r002', 'p004');

insert into s_user (id, id_role, username, email, active) values
('u001', 'r001', 'testmanager', 'testmanager@example.com', true);

insert into s_user (id, id_role, username, email, active) values
('u002', 'r002', 'teststaff', 'teststaff@example.com', true);

-- password : rahasia
insert into s_user_password (id, id_user, hashed_password) values
('up001', 'u001', '$2a$10$XSLWKxslG628cFas6BbZ6uenvcTs9uxa5fw0ZVgipYKlrboUZHlDm');

-- password : cobacoba
insert into s_user_password (id, id_user, hashed_password) values
('up002', 'u002', '$2a$10$BucJJkaqhRyKT4H8Yhv33u.suipe0dbmSEfWCmr9J9PnzyzZD76SS');
