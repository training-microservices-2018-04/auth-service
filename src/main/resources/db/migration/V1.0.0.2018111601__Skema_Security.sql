create table s_role (
  id varchar (36),
  name varchar (100) not null,
  description varchar (255),
  primary key (id),
  unique (name)
);

create table s_user (
  id varchar (36),
  username varchar (100) not null,
  email varchar (100) not null,
  active boolean not null,
  id_role varchar (36) not null,
  primary key (id),
  unique (username),
  unique (email),
  foreign key (id_role) references s_role(id)
);

create table s_user_password (
  id varchar (36),
  id_user varchar (36) not null,
  hashed_password varchar (255) not null,
  primary key (id),
  foreign key (id_user) references s_user(id)
);

create table s_permission (
  id varchar (36),
  permission_label varchar (255) not null,
  permission_value varchar (255) not null,
  primary key (id),
  unique (permission_label),
  unique (permission_value)
);

create table s_role_permission (
  id_role varchar (36) not null,
  id_permission varchar (36) not null,
  primary key (id_role, id_permission),
  foreign key (id_role) references s_role(id),
  foreign key (id_permission) references s_permission(id)
);
