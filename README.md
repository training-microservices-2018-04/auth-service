# Aplikasi Auth Server #

## Cara mendapatkan access token ##

1. Akses URL untuk Otorisasi

        http://localhost:8080/oauth/authorize?client_id=client001&response_type=code

2. Kalau belum login, maka akan disuruh login dulu

    [![Login Page](docs/login-page.png)](docs/login-page.png)

3. Consent screen akan tampil. Pilih scope yang kita setujui. Scope ini nilainya bebas. Terserah aplikasi client dan resource server apakah scope ini akan diperiksa atau tidak.

    [![Consent Screen](docs/consent-screen.png)](docs/consent-screen.png)

4. Kita akan diredirect ke url sesuai `redirect_uri` yang didaftarkan untuk `client001`. Misalnya seperti ini

        http://localhost:10000/handle-oauth-callback?code=5RDPic

5. Gunakan aplikasi http client seperti `curl` atau `Postman` untuk menukar `code` menjadi `token`

        curl -X POST \
        -H "Accept: application/json" \
        -u "client001:abcd" \
        -d "client_id=client001&grant_type=authorization_code&code=5RDPic" \
        http://localhost:8080/oauth/token

    Outputnya seperti ini
    
    ```json
    {
      "access_token":"0f52f8be-9ef4-4642-83d4-6faae790dc5a",
      "token_type":"bearer",
      "refresh_token":"d1982971-048b-4cfd-95ce-12169a0815c7",
      "expires_in":43199,
      "scope":"entri_data review_transaksi approve_transaksi"
    }
    ```
    
    Atau menggunakan Postman seperti ini
    
    [![Postman Authcode](docs/postman-authcode.png)](docs/postman-authcode.png)

6. Tukarkan `access_token` menjadi informasi tentang user dan tokennya

        curl -X POST \
        -H "Accept: application/json" \
        -u "client001:abcd" \
        -d "token=0f52f8be-9ef4-4642-83d4-6faae790dc5a" \
        http://localhost:8080/oauth/check_token
    
    Hasilnya seperti ini
    
    ```json
    {
      "aud":["belajar"],
      "user_name":"teststaff",
      "scope":["entri_data","review_transaksi","approve_transaksi"],
      "active":true,
      "exp":1542395313,
      "authorities":["VIEW_PESERTA","VIEW_SUBJEK"],
      "client_id":"client001"
    }
    ```
    
    Atau menggunakan Postman seperti ini
    
    [![Postman Check Token](docs/postman-check-token.png)](docs/postman-check-token.png)

## Konversi Token dari Opaque menjadi JWT

1. Generate key pair (private & public key) yang tersimpan dalam keystore. Format keystore yang lama adalah `jks`, sekarang sudah deprecated. Rekomendasinya saat ini kita menggunakan `PKCS12`

        keytool -genkey -alias authserver \
            -keystore src/main/resources/authserver.pfx \
            -storetype PKCS12 \
            -keyalg RSA \
            -keysize 2048 
    
    Kita akan ditanyai beberapa pertanyaan sebagai berikut:
    
        Enter keystore password:  
        Re-enter new password: 
        What is your first and last name?
          [Unknown]:  Aplikasi Auth Server
        What is the name of your organizational unit?
          [Unknown]:  Training Division
        What is the name of your organization?
          [Unknown]:  ArtiVisi
        What is the name of your City or Locality?
          [Unknown]:  Jakarta
        What is the name of your State or Province?
          [Unknown]:  DKI Jakarta
        What is the two-letter country code for this unit?
          [Unknown]:  ID
        Is CN=Aplikasi Auth Server, OU=Training Division, O=ArtiVisi, L=Jakarta, ST=DKI Jakarta, C=ID correct?
          [no]:  yes

2. Pasang di konfigurasi Auth Server

    ```java
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authenticationManager(authenticationManager)
        .tokenStore(new JdbcTokenStore(dataSource))
        .accessTokenConverter(jwtTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter jwtTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        KeyPair keyPair = new KeyStoreKeyFactory(
                keystoreFile, keystorePassword.toCharArray())
                .getKeyPair(keypairAlias);
        converter.setKeyPair(keyPair);
        return converter;
    }
    ```
3. Access token sudah berubah menjadi JWT, jalankan dengan prosedur seperti di atas, hasilnya akan menjadi seperti ini

    ```json
    {
        "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmVsYWphciJdLCJ1c2VyX25hbWUiOiJ0ZXN0c3RhZmYiLCJzY29wZSI6WyJlbnRyaV9kYXRhIiwicmV2aWV3X3RyYW5zYWtzaSJdLCJleHAiOjE1NDIzOTc3NjksImF1dGhvcml0aWVzIjpbIlZJRVdfUEVTRVJUQSIsIlZJRVdfU1VCSkVLIl0sImp0aSI6ImE3NjA3NTVhLTFmNTQtNDc2YS1hYmRhLTgwMjUxNDJlM2RhYiIsImNsaWVudF9pZCI6ImNsaWVudDAwMSJ9.pnDZMfXrhwXwHkAD-A0HSazMGBFG5Ypb3CKxF06Si_0jLGvRwGN7-2SmtSGuP2HkwbYutCIkC2gu_6SB8pLKVaP2RPmvlNJt_jHWDqBLRnMUBKkUNtK5iOHjS3921h2Z1q_wIL4ZLTHHME42-sOniPvo7Ea26QWvZVwqGtVyQDAMGd-aJOW4PGVS0xV2kYIOwTDnq_GdYRLEYE925U1SGpH7f-GDWZiBvkasQIq6I3TwzFalqM8-S-rJa8oNXBOhEZOCzqoLNhRXZJ_tSIPlZ2nExdRGDcmwz_I69GvgANjQ-f95O9zmaF4Si39PfIsT5apxx9ubvBwNNPrHjD45ig",
        "token_type": "bearer",
        "refresh_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmVsYWphciJdLCJ1c2VyX25hbWUiOiJ0ZXN0c3RhZmYiLCJzY29wZSI6WyJlbnRyaV9kYXRhIiwicmV2aWV3X3RyYW5zYWtzaSJdLCJhdGkiOiJhNzYwNzU1YS0xZjU0LTQ3NmEtYWJkYS04MDI1MTQyZTNkYWIiLCJleHAiOjE1NDQ5NDY1NjksImF1dGhvcml0aWVzIjpbIlZJRVdfUEVTRVJUQSIsIlZJRVdfU1VCSkVLIl0sImp0aSI6Ijk4NjlhODU2LTVhOGYtNDdiOS1hYWRhLWZhZGFkMDQyYjg5ZCIsImNsaWVudF9pZCI6ImNsaWVudDAwMSJ9.Cezn-CvTeLa1B_KSNUfwnXTyMBX4ZwCbZYwjfrGRn32nH2o6AmiT89PU0pNIZMi31a-dFSIHpaouQbbVJBFuP-9GRHvSqdH8ZtQ-iqdq9i9yfN8Q1HQGED0ClVEc9r3zrh3JAvM-ZHm8jB_JsabmaHmae-YgVcJIqLdQ42Fvih2ACFJy8ss3WWGvo5AcPXQHeFkMxuSqeSomBipC9UcT6pQoqeJHzxZAOuQNzKVhETixAWDkTaVCUv1bQMO5ylOhDyB7cyMAoA5IwDM1FSWJ5iI4b2falmwK6MhPPh394AnksyYSQS8_PNONKxpHIWZF99XvJb5gPWW5y6e4EV77UA",
        "expires_in": 43199,
        "scope": "entri_data review_transaksi",
        "jti": "a760755a-1f54-476a-abda-8025142e3dab"
    }
    ```
4. Token JWT bisa dilihat isinya di website [https://jwt.io](https://jwt.io)

    [![JWT Decode](docs/jwt-decode.png)](docs/jwt-decode.png)

5. Keabsahan JWT bisa diverifikasi dengan memasukkan public key dari server yang menerbitkan JWT tersebut. 

    [![Verifikasi JWT](docs/jwt-verify.png)](docs/jwt-verify.png)

    Untuk aplikasi authserver spring, public key bisa diakses di [http://localhost:8080/oauth/token_key](http://localhost:8080/oauth/token_key) seperti ini

    [![Public Key JWT](docs/public-key-jwt.png)](docs/public-key-jwt.png)